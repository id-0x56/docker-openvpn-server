# Docker
```
sudo apt update \
    && sudo apt install -y curl \
    && curl -fsSL https://get.docker.com -o get-docker.sh \
    && sudo sh get-docker.sh
```
# OpenVPN Server
```
sudo apt update \
    && sudo apt install -y openvpn easy-rsa git
```
```
git clone https://gitlab.com/id-0x56/docker-openvpn.git
```
```
cd docker-openvpn \
    && rm -rf conf/openvpn/easy-rsa
```
```
cd conf/openvpn \
    && make-cadir easy-rsa \
    && cd easy-rsa
```
```
sh easyrsa init-pki \
    && sh easyrsa build-ca \
    && sh easyrsa gen-crl \
    && sh easyrsa gen-dh \
    && /usr/sbin/openvpn --genkey secret pki/ta.key > /dev/null || /usr/sbin/openvpn --genkey --secret pki/ta.key > /dev/null
```
```
sudo ln -s /usr/share/easy-rsa/x509-types pki/x509-types
```
```
sh easyrsa gen-req server nopass \
    && sh easyrsa sign-req server server
```
```
cd ../../..
```
```
sudo docker run --interactive --tty --cap-add=NET_ADMIN --env TZ=Europe/Moscow --volume /etc/localtime:/etc/localtime:ro --volume $(pwd)/conf/openvpn/server:/etc/openvpn/server --volume $(pwd)/conf/openvpn/easy-rsa:/etc/openvpn/easy-rsa --volume $(pwd)/conf/openvpn/ccd:/etc/openvpn/ccd --volume $(pwd)/log/openvpn-status.log:/var/log/openvpn/openvpn-status.log --workdir /etc/openvpn --device=/dev/net/tun --publish 1194:1194/udp --detach --restart unless-stopped 00x56/docker-openvpn
```
```
sudo docker ps -a
```
# OpenVPN Client
```
cd conf/openvpn/easy-rsa
```
```
sh easyrsa gen-req user nopass \
    && sh easyrsa sign-req client user
```
```
clear && ip addr
```
```
clear && cat pki/ta.key
```
```
clear && cat pki/ca.crt
```
```
clear && cat pki/issued/user.crt
```
```
clear && cat pki/private/user.key
```
# OpenVPN Revoke
```
cd conf/openvpn/easy-rsa
```
```
sh easyrsa revoke user \
    && sh easyrsa gen-crl
```
